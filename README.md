# ics-ans-role-ioc

Ansible role to install an IOC.

The `iocs` variable is a list of iocs to instantiate on the system. It should conform to the following structure:
```
iocs:
  - name: <name of the IOC>
    git: <git repository of IOC configuration, default: {{ ioc_git_url }}/{{ ioc_git_namespace }}/{{ name }} >
    version: <git repository version, e.g. tag, branch or commit id, default: HEAD >
    env: <dictionary that will be resolved into environment variables in the IOC environment, default: {} >
```

## Role Variables

```yaml
ioc_packages:
  - git
  - procServ
  - logrotate
  - nfs-utils
# Variable can be used to add extra packages for development
ioc_dev_packages: []

ioc_user_name: iocuser
ioc_user_id: "1042"
ioc_group_name: iocgroup
ioc_group_id: "1042"

ioc_ca_port: 5064

ioc_git_url: https://gitlab.esss.lu.se
ioc_git_namespace: ioc
ioc_overwrite: false

ioc_iocs_folder: /opt/iocs

ioc_conda_envs_directory: /home/{{ ioc_user_name }}/.conda/envs

iocs: []

# hostname of the nonvolatile server
# If not empty, the directory /export/nonvolatile/{{ ioc.name }} will be created
# on that machine and will be mounted locally
# We use "" string by default so that it's easy to disable at host level
ioc_nonvolatile_server: ""
# Local nonvolatile base directory
ioc_nonvolatile_path: /opt/nonvolatile
# Remote nonvolatile base directory
ioc_nonvolatile_server_path: "/export/nonvolatile"

# The following variables can be set to override the defaults from e3-common
# module:
# ioc_log_server_name: localhost
# ioc_errorlog_server_port: "9001"
# ioc_caputlog_server_port: "9001"
# ioc_asg_filename: "unrestricted_access.acf"
# ioc_facility_name: ""

ioc_prometheus_exporter_directory: /opt/e3-exporter
ioc_prometheus_exporter_user: e3-exporter
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc
```

## License

BSD 2-clause
